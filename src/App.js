import React, { Component } from 'react';
import Lottery from './components/Lottery/Lottery'

class App extends Component {
    state = {
        numbers : []
    };

    getRandomNumbers = () => {
        let arr = [];

        while (arr.length < 5) {
            const num =  Math.floor(Math.random() * (36 - 5 ) + 5);
            if(arr.indexOf(num) === -1) {
                arr.push(num)
            }
        }
        arr.sort(function(a, b) {
            return a - b;
        });
        this.setState({numbers: arr})
    };

  render() {
    return (
        <React.Fragment>
            <div className="btn-center">
                <button className="btn" onClick={this.getRandomNumbers}>Change Number</button>
            </div>
          <div className="lottery-circle">
              <Lottery number={this.state.numbers[0]}/>
              <Lottery number={this.state.numbers[1]}/>
              <Lottery number={this.state.numbers[2]}/>
              <Lottery number={this.state.numbers[3]}/>
              <Lottery number={this.state.numbers[4]}/>
          </div>

        </React.Fragment>

    );
  }
}

export default App;
